DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS user_assessment;
DROP TABLE IF EXISTS assessment_answer;
DROP TABLE IF EXISTS section_result;
DROP TABLE IF EXISTS assessment_result;

CREATE TABLE user (
  user_id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL,
  membership_type VARCHAR(250) NOT NULL,
  status VARCHAR(250) NOT NULL,
  creation_date DATE NOT NULL
);

CREATE TABLE user_assessment (
  assessment_id INT AUTO_INCREMENT  PRIMARY KEY,
  user_id INT NOT NULL,
  creation_date DATE NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE
);

CREATE TABLE assessment_answer (
  answer_id INT AUTO_INCREMENT  PRIMARY KEY,
  assessment_id INT NOT NULL,
  question_type VARCHAR(250) NOT NULL,
  relevancy INT NOT NULL,
  answer INT DEFAULT NULL,
  FOREIGN KEY (assessment_id) REFERENCES user_assessment(assessment_id) ON DELETE CASCADE
);

CREATE TABLE assessment_result (
  result_id INT AUTO_INCREMENT  PRIMARY KEY,
  assessment_id INT NOT NULL,
  result INT NOT NULL,
  level INT NOT NULL,
  FOREIGN KEY (assessment_id) REFERENCES user_assessment(assessment_id) ON DELETE CASCADE
);

CREATE TABLE section_result (
  assessment_section_id INT AUTO_INCREMENT  PRIMARY KEY,
  section_id INT NOT NULL,
  result_id INT NOT NULL,
  section_result INT NOT NULL,
  FOREIGN KEY (result_id) REFERENCES assessment_result(result_id) ON DELETE CASCADE
);

INSERT INTO user (name, email, membership_type, creation_date) VALUES
  ('Hagar', 'hagar@gmail.com', 'yearly', curdate()),
  ('Test', 'test@gmail.com', 'monthly', curdate());